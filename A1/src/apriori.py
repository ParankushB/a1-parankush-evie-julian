#Assignment 1
#Author: Parankush, Julian, and Evie
#Date: September 18, 2018
import itertools

#Computes the support of the given itemset in the given database.
#itemset: A set of items
#database: A list of sets of items
#return: The number of sets in the database which itemset is a subset of. 
def support(itemset, database):
   
    numberOfSubsets = 0

    if isinstance(itemset, str):
        itemset = list([itemset])

    counter = len(itemset)
    for record in database:
        for element in itemset:
            if element in record:
                counter = counter - 1
        if (counter == 0):
            numberOfSubsets = numberOfSubsets + 1

    return numberOfSubsets

#Computes the confidence of a given rule.
#The rule takes the form precedent --> antecedent
#precedent: A set of items
#antecedent: A set of items that is a superset of precedent
#database: a list of sets of items.
#return: The confidence in precedent --> antecedent.
def confidence(precedent, antecedent, database):    
    xPlusY = []

    for item in precedent:
        xPlusY.append(item)

    for item in antecedent:
        xPlusY.append(item)
    
    supportXPlusY = float(support(xPlusY, database))
    supportX = float(support(precedent, database))
    
    if supportX == 0.0:
        supportX = 0.01
    confidence = supportXPlusY / supportX

    return confidence

#Finds all itemsets in database that have at least minSupport.
#database: A list of sets of items.
#minSupport: an integer > 1
#return: A list of sets of items, such that 
#s in return --> support(s,database) >= minSupport.
def findFrequentItemsets(database, minSupport):
    
    itemset = []

    for record in database:
        for feature in record:
            itemset.append(feature)
    
    itemset = set(itemset)
    newitemset = []

    for item in itemset:
        newitemset.append(item)

    itemset = newitemset
    candidates = itemset

    frequentSet = []

    while len(candidates) > 0:

        hypothesis = []

        for candidate in candidates:

            if support(candidate, database) >= minSupport:
                hypothesis.append(candidate)

        frequentSet.append(hypothesis) 
    
        candidates = []

        hypocopy = hypothesis

        for x in hypocopy:
            hypocopy.remove(x)
            for y in hypocopy:
                newSet = [x, y] 
                candidates.append(newSet)
    
    return frequentSet


#Given a set of frequently occuring Itemsets, returns
# a list of pairs of the form (precedent, antecedent)
# such that for every returned pair, the rule 
# precedent --> antecedent has confidence >= minConfidence
# in the database.
#frequentItemsets: a set or list of sets of items.
#database: A list of sets of items.
#minConfidence: A real value between 0.0 and 1.0. 
#return: A set or list of pairs of sets of items. 
def findRules(frequentItemsets, database, minConfidence):
    rules = []

    for item in frequentItemsets:
        for otherItem in frequentItemsets:
            if item != otherItem:
                if confidence(item, otherItem, database) >= minConfidence:
                    rules.append([item, otherItem])

    return rules

#Produces a visualization of frequent itemsets.
def visualizeItemsets(frequentItemsets, database):
    for item in frequentItemsets:
        for entity in item:
            print("frequent item set:", entity)

#Produces a visualization of rules.
def visualizeRules(frequentItemsets, database):
    for r in findRules(frequentItemsets, database, 0.6):
        print(str(r[0])+"  ===>   "+ str(r[1]))
   
#produces the database from local .txt file
def createDatabase():
    database = []
    database2 = []
    newLine = str('\n')

    finalDatabase = []
    
    with open('first5.txt', 'r') as file:
        for f in file.readlines():
            database.append(f)

    for record in database: 
        indexOfFeature = 0
        feature = record.split('\n')

        for f in feature:
            element = f.split(',')
            newSet = []
        
            for e in element:

                newElement = str("feature" + str(indexOfFeature) + '=' + str(e))
                newSet.append(newElement)
                indexOfFeature = indexOfFeature + 1

            if newSet not in finalDatabase:
                finalDatabase.append(newSet)

    return finalDatabase

database = createDatabase()
frequentItemSets = findFrequentItemsets(database, 5)
visualizeItemsets(frequentItemSets, database)
visualizeRules(frequentItemSets, database)
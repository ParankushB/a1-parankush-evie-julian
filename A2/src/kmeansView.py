import pylab as pl
import numpy as np
from sklearn.cluster import KMeans

file = open("newHeadLess.csv")
file.readline()
data = np.loadtxt(fname = file, delimiter = ',')

kmeans = KMeans(n_clusters=2)
kmeans.fit(data)
y_kmeans = kmeans.predict(data)

pl.scatter(data[:, 30], data[:, 32], c=y_kmeans, s=50, cmap='winter')

centers = kmeans.cluster_centers_
pl.scatter(centers[:, 30], centers[:, 32], c='black', s=200, alpha=0.5)
pl.show() 

#29, 32, Linalool

#b-Caryophyllene

import java.util.*
import java.io.*


var dataset = ArrayList<HashMap<String, Any?>>();
var featureList = ArrayList<String>()

fun main(args: Array<String>){  

    datapreperation()

    dendogram(convertToTree(dataset))
}

fun datapreperation() {
    val filename = "strainlessData.csv"
    val file = File(filename)
    val sc = Scanner(file)
    
    var st = sc.nextLine()
    var features = st.split(",")

    featureList = ArrayList<String>(features)

    var data = ArrayList<HashMap<String, Any?>>()

    while(sc.hasNextLine() ){
        var currentLine = sc.nextLine().split(",")

        val datapoint = HashMap<String, Any?>()

        for ((index, feature) in features.withIndex()) {

            val featureAsString= feature.toString()

            if (currentLine[index].equals("ND")){
                datapoint.put(featureAsString, null)
            } else {
                datapoint.put(featureAsString, currentLine[index].toFloat())
            }
        }
        data.add(datapoint)
    }
    dataset = data
}

//Accepts two data points a and b.
//Returns the distance between a and b.
//Note that this might be specific to your data.
fun distance(a: HashMap<String, Any?>, b: HashMap<String, Any?>): Double {

    var distance = 0.0
    
    for (feature in featureList) {

        //find the value from both maps
        val aValue = a.get(feature)
        val bValue = b.get(feature)

        //catch nulls
        if (aValue != null && bValue != null && aValue is Float && bValue is Float) {

            //then, get the difference between both values
            var subDistance = bValue - aValue
            subDistance = subDistance * subDistance

            //add them to distance
            distance += subDistance
        }
    }

    return Math.sqrt(distance);
}


//Accepts two data points a and b.
//Produces a point that is the average of a and b.
fun merge(a: HashMap<String, Any?>, b: HashMap<String, Any?>): HashMap<String, Any?> {

    var newPoint = HashMap<String, Any?>()

    for (feature in featureList) {
        
        //find the value from both maps
        val aValue = a.get(feature)
        val bValue = b.get(feature)

        //catch nulls
        if (aValue != null && bValue != null && aValue is Float && bValue is Float) {
            val average = (aValue + bValue)/2
            newPoint.put(feature, average)
        }
    }
    return newPoint
}

//Accepts a list of data points.
//Returns the pair of points that are closest
fun findClosestPair(listOfPoints: ArrayList<HashMap<String, Any?>>): ArrayList<HashMap<String, Any?>> {

    var closestPair = ArrayList<HashMap<String, Any?>>()
    var minDistance = Integer.MAX_VALUE

    //loop through every 2 pair combination in the list (O(N^2))
    for (xPoint in listOfPoints) {
        for (yPoint in listOfPoints) {
            if (xPoint != yPoint) {

                val distance = distance(xPoint, yPoint)

                if (distance < minDistance) {
                    closestPair.clear()
                    closestPair.add(xPoint)
                    closestPair.add(yPoint)
                }
            }
        }
    }
    return closestPair
}

//Accepts a list of data points.
//Returns the pair of points that are closest
fun findClosestNodePairs(listOfNodes: ArrayList<TreeNode<HashMap<String, Any?>>>): ArrayList<TreeNode<HashMap<String, Any?>>> {

    var closestPair = ArrayList<TreeNode<HashMap<String, Any?>>>()
    var minDistance = Integer.MAX_VALUE

    //loop through every 2 pair combination in the list (O(N^2))
    for (xPoint in listOfNodes) {
        for (yPoint in listOfNodes) {
            if (xPoint != yPoint) {

                val distance = distance(xPoint.value, yPoint.value)

                if (distance < minDistance) {
                    closestPair.clear()
                    closestPair.add(xPoint)
                    closestPair.add(yPoint)
                }
            }
        }
    }
    return closestPair
}

fun convertToTree(dataset: ArrayList<HashMap<String, Any?>>): ArrayList<TreeNode<HashMap<String, Any?>>> {
   
    var listOfNodes = ArrayList<TreeNode<HashMap<String, Any?>>>()

    for (datum in dataset) {
        var node = TreeNode<HashMap<String, Any?>>(datum)
        listOfNodes.add(node)
    }
    return listOfNodes
}

//produces a tree given a list of pairs
fun dendogram(treelist: ArrayList<TreeNode<HashMap<String, Any?>>>) {
    
    var centers = treelist

    while (centers.size > 1) {

        //get the closest two pairs
        val location = findClosestNodePairs(centers)

        //remove them from list of centers
        centers.remove(location.get(0))
        centers.remove(location.get(1))

        //create a new center that's the avg of the two closest pairs
        val newCluster = merge(location.get(0).value, location.get(1).value)

        var tree = TreeNode(newCluster)
        tree.addChild(location.get(0))
        tree.addChild(location.get(1))


        //add that to the list of clusters
        centers.add(tree)
    }

    print(centers.get(0).value)
}

class TreeNode<HashMap>(value: HashMap){
    var value = value

    var parent: TreeNode<HashMap>? = null

    var children: MutableList<TreeNode<HashMap>> = mutableListOf()

    fun addChild(node:TreeNode<HashMap>){
        children.add(node)
        node.parent = this
    }
}



//Accepts a list of data points.
//Produces a tree structure corresponding to a 
//Agglomerative Hierarchal clustering of D.
fun HClust(listOfPoints: ArrayList<HashMap<String, Any?>>) {

    var centers = listOfPoints

    var splits = ArrayList<ArrayList<HashMap<String, Any?>>>()

    while (centers.size > 1) {

        //get the closest two pairs
        val location = findClosestPair(centers)

        //remove them from list of centers
        centers.remove(location.get(0))
        centers.remove(location.get(1))

        //create a new center that's the avg of the two closest pairs
        val newCluster = merge(location.get(0), location.get(1))

        //add that to the list of clusters
        centers.add(newCluster)
        
        splits.add(location)

    }
}
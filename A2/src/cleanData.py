    
database = []
newData = []

def findAverage(database, index):
    sum = 0 
    counter = 0 
    for element in database:
        if element[index] != 'ND' and element[index] != 'ND\n':
            sum += float(element[index])
            counter = counter + 1

    average = sum / counter 

    return average

 
with open('headless.csv', 'r') as file:
    for f in file.readlines():
        database.append(f)
    
for record in database:
    feature = record.split(',')
    newData.append(feature)

for column in range(41):
    for row in newData:
        if row[column] == 'ND':
            row[column] = str(findAverage(newData, column))
        elif row[column] == 'ND\n':
            row[column] = str(findAverage(newData, column)) + '\n'

with open('newHeadLess.csv', 'w') as file:

    for element in newData:
        finalElement = ','.join(map(str, element)) 
        file.write(finalElement)
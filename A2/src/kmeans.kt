
import java.util.*
import java.io.*

var dataset = ArrayList<HashMap<String, Any?>>();
var featureList = ArrayList<String>()

fun main(args: Array<String>){  

    dataprep()
    val result = KMeans(dataset, 3)
    print(result)
}

fun dataprep() {
    val filename = "strainlessData.csv"
    val file = File(filename)
    val sc = Scanner(file)
    
    var st = sc.nextLine()
    var features = st.split(",")

    featureList = ArrayList<String>(features)

    var data = ArrayList<HashMap<String, Any?>>()

    while(sc.hasNextLine() ){
        var currentLine = sc.nextLine().split(",")

        val datapoint = HashMap<String, Any?>()

        for ((index, feature) in features.withIndex()) {

            val featureAsString= feature.toString()

            if (currentLine[index].equals("ND")){
                datapoint.put(featureAsString, null)
            } else {
                datapoint.put(featureAsString, currentLine[index].toFloat())
            }
        }
        data.add(datapoint)
    }
    dataset = data
}

// # Accepts two data points a and b.
// # Returns the euclidian distance 
// # between a and b.
fun euclidianDistance(a: HashMap<String, Any?>, b: HashMap<String, Any?>): Double {

    var distance = 0.0
    
    for (feature in featureList) {

        //find the value from both maps
        val aValue = a.get(feature)
        val bValue = b.get(feature)

        //catch nulls
        if (aValue != null && bValue != null && aValue is Float && bValue is Float) {

            //then, get the difference between both values
            var subDistance = bValue - aValue
            subDistance = subDistance * subDistance

            //add them to distance
            distance += subDistance
        }
    }

    return Math.sqrt(distance)
}


// # Accepts a list of data points D, and a list of centers
// # Returns a dictionary of lists called "clusters", such that
// # clusters[c] is a list of every data point in D that is closest
// #  to center c.
fun assignClusters(listOfDataPoints: ArrayList<HashMap<String, Any?>>, centers: ArrayList<HashMap<String, Any?>>): HashMap<HashMap<String, Any?>, ArrayList<HashMap<String, Any?>>> {

    var clusters = HashMap<HashMap<String, Any?>, ArrayList<HashMap<String, Any?>>>()

    for (c in centers) {
        clusters.put(c, arrayListOf())
    }

    for (point in listOfDataPoints) {
        var closestCenter = HashMap<String, Any?>()
        var smallestDistance = Double.MAX_VALUE

        for (center in centers) {
            val newDistance = euclidianDistance(point, center)
            if (newDistance < smallestDistance) {
                smallestDistance = newDistance
                closestCenter = center
            }   
        }

        var currentValue = clusters.get(closestCenter)

        if (currentValue != null) {
            currentValue.add(point)
            clusters.put(closestCenter, currentValue)
        }
    }

    return clusters
}

// # Accepts a list of data points.
// # Returns the mean of the points.
fun findClusterMeans(listOfDataPoints: ArrayList<HashMap<String, Any?>>): HashMap<String, Any?> {

    var newPoint = HashMap<String, Any?>()

    for (feature in featureList) {
        var sumOfValues = 0.0f
        var legitPoints = 0

        for (point in listOfDataPoints) {
            val value = point.get(feature)
            if (value != null && value is Float) {
                legitPoints = legitPoints + 1
                sumOfValues = sumOfValues + value
            }
        }
        val average = sumOfValues / legitPoints
        newPoint.put(feature, average)
    }
    return newPoint
}

// # Accepts a list of data points, and a number of clusters.
// # Produces a set of lists representing a K-Means clustering
// #  of D.
fun KMeans(listOfDataPoints: ArrayList<HashMap<String, Any?>>, clusterAmount: Int): HashMap<HashMap<String, Any?>, ArrayList<HashMap<String, Any?>>> {

    var newClusters = HashMap<HashMap<String, Any?>, ArrayList<HashMap<String, Any?>>>()

    var means = ArrayList<HashMap<String, Any?>>()

    for (amount in 0..clusterAmount - 1) {
        means.add(listOfDataPoints[amount])
    }


    var oldMeans = means

    do {
        oldMeans = means    

        //find clusters
        newClusters = assignClusters(listOfDataPoints, means)

        val newMeans = ArrayList<HashMap<String, Any?>>()

        //get means of each cluster
        for (mean in means) {
            val cluster = newClusters.get(mean)

            if (cluster != null) {
                val newPoint = findClusterMeans(cluster)
                newMeans.add(newPoint)
            }
        }

        means = newMeans

    } while(oldMeans != means)

    return newClusters

}
import pylab as pl
import numpy as np
from sklearn.cluster import KMedoids

file = open("newHeadLess.csv")
file.readline()
data = np.loadtxt(fname = file, delimiter = ',')

kmeans = KMedoids(n_clusters=2)
kmeans.fit(data)
y_kmeans = kmeans.predict(data)

pl.scatter(data[:, 0], data[:, 1], c=y_kmeans, s=50, cmap='winter')

centers = kmeans.cluster_centers_
pl.show()
